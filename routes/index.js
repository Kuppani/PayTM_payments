const router = require('express').Router();
const testConfig = require('../config/test-config');
/* GET home page. */
router.get('/', (req, res)=> {
  // res.redirect('THis is the payments api route');
  res.locals.config = testConfig;
  res.render('testtxn')
});

module.exports = router;