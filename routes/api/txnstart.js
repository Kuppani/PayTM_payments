const liveConfig = require('../../config/live-config');
const testConfig = require('../../config/test-config');
const paymentMethod = require('../../methods/payment');

const router = require('express').Router();

//Staging
router.post('/test', (req, res) => {
	const stageOptions = {
		config: testConfig
	}
	paymentMethod.startTxn(req, res, stageOptions);
});

//Production
router.post('/live', (req, res) => {
	const prodOptions = {
		config: liveConfig
	}
	paymentMethod.startTxn(req, res, prodOptions);
});

module.exports = router;