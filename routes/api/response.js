const checksum = require('../../model/checksum');
const config = require('../../config/config');
const router = require('express').Router();

// route - /response
router.post('/', (req, res) => {
	const paramlist = req.body;
	if (checksum.verifychecksum(paramlist, config.PAYTM_MERCHANT_KEY)) {
		paramlist.validation = 'successful';
	} else {
		paramlist.validation = 'NOT successful';
	};
	res.send(paramlist)
});

module.exports = router;