const router = require('express').Router();

//route - /pgredirect
router.get('/', (req, res) => {
	console.log("\nin pgredirect\n");
	res.locals.callback = `${req.protocol}://${req.get('host')}/response`;
	res.render('pgredirect');
});

module.exports = router;