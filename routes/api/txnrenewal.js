const request = require('request');
const router = require('express').Router();
const checksum = require('../../model/checksum');

const methods = require('../../methods/payment');

const liveConfig = require('../../config/live-config');
const testConfig = require('../../config/test-config');

//ROute - /api/payments/txnrenewal
router.post('/test', (req, res) => {
    const testOptions = {
        config: testConfig
    }
    methods.renewTxn(req, res, testOptions);
});
router.post('/live', (req, res) => {
    const prodOptions = {
        config: liveConfig
    }
    methods.renewTxn(req, res, prodOptions);
});

module.exports = router;