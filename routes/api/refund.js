const request = require('request');
const router = require('express').Router();

const checksum = require('../../model/checksum');
const testConfig = require('../../config/test-config');
const liveConfig = require('../../config/live-config');

const methods = require('../../methods/payment');

const productionURL = 'https://secure.paytm.in/oltp/HANDLER_INTERNAL/REFUND';
const stagingURL = 'https://pguat.paytm.com/oltp/HANDLER_INTERNAL/REFUND';

const testOptions = {
	config: testConfig,
	url: stagingURL
};
const liveOptions = {
	config: liveConfig,
	url: productionURL
};

//Route - /api/payments/refund/
router.post('/test', (req, res) => {
	methods.startRefundProcess(req, res, testOptions)
});

router.post('/live', (req, res) => {
	methods.startRefundProcess(req, res, liveOptions)
});

module.exports = router;