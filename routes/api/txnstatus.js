const request = require('request');
const router = require('express').Router();
const checksum = require('../../model/checksum');


const methods = require('../../methods/payment');

const stagingURL = "https://pguat.paytm.com/oltp/HANDLER_INTERNAL/getTxnStatus?";
const productionURL = "https://secure.paytm.in/oltp/HANDLER_INTERNAL/getTxnStatus?";

const liveConfig = require('../../config/live-config');
const testConfig = require('../../config/test-config');
const testOptions = {
	config: testConfig,
	url: stagingURL
};
const prodOptions = {
	config: liveConfig,
	url: productionURL
};

router.post('/test', (req, res) => {
	methods.getTxnStatus(req, res, testOptions);
});

router.post('/live', (req, res) => {
	methods.getTxnStatus(req, res, prodOptions);
});

module.exports = router;