const min = 999;
const max = 999999;
const formatDate = function (date) {
    let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

const getRandomString = function (min = 999, max = 999999, str) {
    let value = Math.round((Math.random() * (max - min)) + min);
    return `${str}${value}`;
}

const randOrder = function (min, max) {
    return getRandomString(min, max, 'order');
}

const randSubs = function (min, max) {
    return getRandomString(min, max, 'subsOrder');
}

module.exports = {
    formatDate,
    getRandomString,
    randOrder,
    randSubs
}