'use strict';
require('dotenv').config();
const method = require('./methods');

const PAYTM_STAG_URL = 'https://pguat.paytm.com/oltp-web/processTransaction';
const PAYTM_PROD_URL = 'https://secure.paytm.in/oltp-web/processTransaction';

let PAYTM_ENVIRONMENT, MID, PAYTM_MERCHANT_KEY, WEBSITE, INDUSTRY_TYPE_ID;

let CHANNEL_ID = 'WEB';
let PAYTM_FINAL_URL = '';
let EMAIL = '';
let d = new Date();

const REQUEST_TYPE = process.env.REQUEST_TYPE;

let ORDER_ID = method.randOrder();
let CUST_ID = method.getRandomString(999,9999,'cmrId');

let SUBS_SERVICE_ID = method.randSubs();
let SUBS_AMOUNT_TYPE = 'FIX'; //POSSIBLE VALUES ARE FIX/VARIABLE
let SUBS_FREQUENCY = 1; // example: 30 if unit is days, -1 if unit is month
let SUBS_FREQUENCY_UNIT = 'DAY'; //POSSIBLE VALUES ARE: DAY/MONTH/YEAR
const SUBS_ENABLE_RETRY = 1; //possible values are 0 or 1
const SUBS_EXPIRY_DATE = method.formatDate(d.setDate(d.getDate() + 7)); //set expirty date to a week from now
let SUBS_PPI_ONLY = 'Y';

const CALLBACK_URL = process.env.CALLBACK_URL;
//strict equality check
if (process.env.ENVIRONMENT === 'TEST') {
  //STAGING SERVER DETAILS
  PAYTM_ENVIRONMENT = 'TEST';   // PROD FOR PRODUCTION
  MID = process.env.TEST_MID;
  PAYTM_MERCHANT_KEY = process.env.TEST_PAYTM_MERCHANT_KEY;
  WEBSITE = process.env.TEST_WEBSITE;
  INDUSTRY_TYPE_ID = process.env.TEST_INDUSTRY_TYPE_ID;
  PAYTM_FINAL_URL =  PAYTM_STAG_URL;
} else {
  //Production server details
  PAYTM_ENVIRONMENT = 'PROD';
  MID = process.env.LIVE_MID;
  PAYTM_MERCHANT_KEY = process.env.LIVE_PAYTM_MERCHANT_KEY;
  WEBSITE = process.env.LIVE_WEBSITE;
  INDUSTRY_TYPE_ID = process.env.LIVE_INDUSTRY_TYPE_ID;
  PAYTM_FINAL_URL =  PAYTM_PROD_URL;
  
}
console.log("suma",MID);

const exportParams = {
  MID,
  PAYTM_MERCHANT_KEY,
  PAYTM_FINAL_URL,
  WEBSITE,
  CHANNEL_ID,
  INDUSTRY_TYPE_ID,
  CALLBACK_URL,
  REQUEST_TYPE,
  CUST_ID,
  ORDER_ID
};

if (REQUEST_TYPE === 'DEFAULT') {
  module.exports = exportParams;
}
else if (REQUEST_TYPE === 'SUBSCRIBE') {
  const b = {
    SUBS_AMOUNT_TYPE,
    SUBS_EXPIRY_DATE,
    SUBS_FREQUENCY,
    SUBS_FREQUENCY_UNIT,
    SUBS_ENABLE_RETRY,
    SUBS_SERVICE_ID,
    SUBS_PPI_ONLY
  }
  module.exports = Object.assign(exportParams,b);
} 
// else if (REQUEST_TYPE === 'RENEW_SUBSCRIPTION')
// {
//   const SUBS_ID = SUBS_SERVICE_ID;
//   console.log("123",SUBS_ID);
//   module.exports = {
//     MID,
//     SUBS_ID
//   }
// }



