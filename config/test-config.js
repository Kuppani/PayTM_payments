'use strict';
require('dotenv').config();
const method = require('./methods');

const PAYTM_ENVIRONMENT = 'TEST'; 
const CHANNEL_ID = 'WEB';
const PAYTM_FINAL_URL = 'https://pguat.paytm.com/oltp-web/processTransaction';
//STAGING SERVER DETAILS
const MID = process.env.TEST_MID;
const PAYTM_MERCHANT_KEY = process.env.TEST_PAYTM_MERCHANT_KEY;
const WEBSITE = process.env.TEST_WEBSITE;
const INDUSTRY_TYPE_ID = process.env.TEST_INDUSTRY_TYPE_ID;
const REQUEST_TYPE = process.env.REQUEST_TYPE;

let d = new Date();
let ORDER_ID = method.randOrder();
let CUST_ID = method.getRandomString(999, 9999, 'cmrId');
let SUBS_SERVICE_ID = method.randSubs();

let SUBS_AMOUNT_TYPE = 'FIX'; //POSSIBLE VALUES ARE FIX/VARIABLE
let SUBS_FREQUENCY = 1; // example: 30 if unit is days, -1 if unit is month
let SUBS_FREQUENCY_UNIT = 'DAY'; //POSSIBLE VALUES ARE: DAY/MONTH/YEAR
const SUBS_ENABLE_RETRY = 1; //possible values are 0 or 1
const SUBS_EXPIRY_DATE = method.formatDate(d.setDate(d.getDate() + 7)); //set expirty date to a week from now
let SUBS_PPI_ONLY = 'Y';

const exportParams = {
    MID,
    PAYTM_MERCHANT_KEY,
    PAYTM_FINAL_URL,
    WEBSITE,
    CHANNEL_ID,
    INDUSTRY_TYPE_ID,
    REQUEST_TYPE,
    CUST_ID,
    ORDER_ID
};

if (REQUEST_TYPE === 'DEFAULT') {
    module.exports = exportParams;
} else if (REQUEST_TYPE === 'SUBSCRIBE') {
    const b = {
        SUBS_AMOUNT_TYPE,
        SUBS_EXPIRY_DATE,
        SUBS_FREQUENCY,
        SUBS_FREQUENCY_UNIT,
        SUBS_ENABLE_RETRY,
        SUBS_SERVICE_ID,
        SUBS_PPI_ONLY
    }
    module.exports = Object.assign(exportParams, b);
}
// else { // if (REQUEST_TYPE === 'RENEW_SUBSCRIPTION')
//   const SUBS_ID = SUBS_SERVICE_ID;
//   module.exports = {
//     MID,
//     SUBS_ID
//   }
// }