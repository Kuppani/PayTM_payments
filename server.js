const app = require('./app');
const port = 8088;

app.listen(port,()=>{
    console.log(`The server has started on ${port}`);
});