const excludeProperties = (obj, keysArr)=> {
    const target = {};
    for (let prop in obj) {
        if (keysArr.indexOf(prop) >= 0) continue;
        if (!obj.hasOwnProperty(prop)) continue;
        target[prop] = obj[prop];
    }
    // console.log('\nTrimmed Object\n',target);
    return target;
};

const arrayToObject = (arr)=>{
    let tempObj = {};
    const keys = Object.keys(arr);
    for(key of keys) {
        tempObj[key] = arr[key];
    };
    return tempObj;
}

module.exports = {
    excludeProperties,
    arrayToObject
}