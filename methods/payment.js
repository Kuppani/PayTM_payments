const request = require('request');
const helper = require('./helper');
const checksum = require('../model/checksum');

//Start the transaxtion
const startTxn = (req, res, options) => {
	console.log("\nPOST Order start\n");

	let paramObj = {};
	const config = options.config;
	let {SUBS_AMOUNT_TYPE,SUBS_FREQUENCY,SUBS_FREQUENCY_UNIT,SUBS_EXPIRY_DATE} = config;
	const propsToAdd = {
		SUBS_AMOUNT_TYPE,
		SUBS_FREQUENCY,
		SUBS_FREQUENCY_UNIT,
		SUBS_EXPIRY_DATE
	};
	const paramlist = Object.assign(config,req.body);
	const PAYTM_MERCHANT_KEY = config.PAYTM_MERCHANT_KEY;
	//reqbody contains
	// SUBS_SERVICE_ID:subsOrder347548
	//SUBS_PPI_ONLY:Y
	// SUBS_ENABLE_RETRY:1
	

	// console.log('paramlist: ', paramlist);
	paramObj = helper.excludeProperties(paramlist,[
		'PAYTM_MERCHANT_KEY',
		'PAYTM_FINAL_URL',
		'SUBSCRIPTION_PLAN_TYPE'
	]);
	paramObj['CALLBACK_URL'] = `${req.protocol}://${req.get('host')}/response`; //get the root url of server
	console.log('paramObj: ', paramObj);
	paramObj.TXN_AMOUNT = '1';
	console.log('PAYTM_MERCHANT_KEY',PAYTM_MERCHANT_KEY);
	checksum.genchecksum(paramObj, PAYTM_MERCHANT_KEY, (err, result) => {
		// console.log('result', result);
		res.locals.restdata = result;
		res.locals.url = config.PAYTM_FINAL_URL;
		res.render('pgredirect');
	});
	console.log("\nPOST Order end\n");
};

//Get transaction status query
const getTxnStatus = function (req, res, options) {
	console.log('TXN_status post start');
	const reqBody = req.body;
	const paramlist = {};
	const config = options.config;
	// console.log('config: ', config);

	paramlist.ORDERID = reqBody.ORDERID;
	paramlist.MID = config.MID;
	const PAYTM_MERCHANT_KEY = config.PAYTM_MERCHANT_KEY;
	console.log('\nparamlist:\n ', paramlist);
	console.log('\nPAYTM_MERCHANT_KEY:\n ', PAYTM_MERCHANT_KEY);

	checksum.genchecksum(paramlist, PAYTM_MERCHANT_KEY, (err, result) => {
		console.log('Genearting checksum...');
		result['CHECKSUMHASH'] = encodeURIComponent(result['CHECKSUMHASH']);
		const finalstring = options.url + 'JsonData=' + JSON.stringify(result);
		console.log('finalstring: ', finalstring);
		request.get(finalstring, (error, response, body) => {
			if (error) {
				console.log(error);
			} else {
				const dataToSend = JSON.parse(body);
				res.send(dataToSend);
			}
		});
	});
	console.log('TXN_status post end');
};

//Renew transaction
const renewTxn = (req, res, options) => {
    console.log("\nRenewal post start\n");

    const config = options.config;
    const reqBody = req.body;
    console.log('reqBody: ', reqBody);
    const paramlist = Object.assign({}, {
        "MID": config.MID,
        "ORDER_ID": reqBody.ORDER_ID,
        "SUBS_ID": reqBody.SUBS_ID,
        "TXN_AMOUNT": reqBody.TXN_AMOUNT,
        "REQUEST_TYPE": 'RENEW_SUBSCRIPTION',
    });
	// "CALLBACK_URL": `${req.protocol}://${req.get('host')}/response`,
    const PAYTM_MERCHANT_KEY = config.PAYTM_MERCHANT_KEY;

    checksum.genchecksum(paramlist, PAYTM_MERCHANT_KEY, (err, result) => {
		console.log('Generating checksum...');
		console.log('result: ', result);
        request.post({
            url: config.PAYTM_FINAL_URL, //URL to hit
            form: result
        }, (error, response, body) => {
            if (error) {
                console.log(error);
            } else {
                console.log(response.statusCode);
                // const response = JSON.parse(body);
                if (response.statusCode == 200) {
                    console.log('body: ', body);
                    res.send(body);
                } else {
                    res.send({
                        'statusCode': response.statusCode,
                        'status': 'Failed!'
                    })
                }
            }
        });
    });
    console.log("\nRenewal post end\n");
}

//Refund method
const startRefundProcess = (req, res, options, txntype = 'REFUND') => {

	console.log("\nRenewal post start\n");
	const reqBody = req.body;
	const config = options.config;
	const finalUrl = options.url;
		
		const paramlist = Object.assign({},{
			"MID":config.MID,
			"ORDERID":reqBody.ORDERID,
			"REFID":reqBody.REFID,
			"REFUNDAMOUNT":reqBody.REFUNDAMOUNT,
			"TXNID":reqBody.TXNID,
			"TXNTYPE":txntype
		})
	const PAYTM_MERCHANT_KEY = config.PAYTM_MERCHANT_KEY;
	checksum.genchecksumforrefund(paramlist, PAYTM_MERCHANT_KEY, (err, result) => {
		console.log('Generating checksum...');
		const finalstring = finalUrl + '?JsonData=' + JSON.stringify(result);
		console.log('\nfinalstring: ', finalstring);
		request.get({
			url: finalstring, //URL to hit
		}, (error, response, body) => {
			if (error) {
				console.log(error);
				res.send(error.message);
			} else {
				console.log(response.statusCode);
				if (response.statusCode == 200) {
					// console.log('response: ', response);
					console.log('body: ', body);
					res.send(body);
				} else {
					res.send({
						'statusCode': response.statusCode,
						'status': 'Failed!'
					});
				}
			}
		});


	});
	console.log("\nRenewal post end\n");

}

module.exports = {
	startTxn,
	getTxnStatus,
	startRefundProcess,
	renewTxn
}