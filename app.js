const express = require("express");
const app = express();
const router = express.Router();
const bodyParser = require('body-parser');

//There is an order for this
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(express.static(__dirname + '/public'));n

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.use('/', router);

//Route variables
const index = require('./routes/index');
const txnstart = require('./routes/api/txnstart');
const txnstatus = require('./routes/api/txnstatus');
const txnrenewal = require('./routes/api/txnrenewal');
const refund = require('./routes/api/refund');
const pgredirect = require('./routes/api/pgredirect');
const response = require('./routes/api/response');

//Routes
app.use('/', index);

app.use('/api/payments/txnstart', txnstart); // /live
app.use('/api/payments/txnstatus', txnstatus);
app.use('/api/payments/txnrenewal', txnrenewal);//This will renew a subscription transaction
app.use('/api/payments/refund', refund); //refund/cancel a particular

app.use('/api/payments/pgredirect', pgredirect); //Our parameters will be submitted to paytm and we are redirected to payment gatewy
app.use('/response', response);

module.exports = app;