"use strict";

const crypt = require('./crypt');
const util = require('util');
const crypto = require('crypto');

//mandatory flag: when it set, only mandatory parameters are added to checksum

const paramsToString = (params, mandatoryflag) => {
	const tempParams = Object.assign({}, params);
	let data = '';
	let flag = 0;
	if (tempParams.TXNTYPE !== undefined || tempParams.TXNTYPE) {
		flag = (tempParams.TXNTYPE === 'REFUND') ? 1 : 0;
	}
	const tempKeys = Object.keys(tempParams);
	console.log('tempKeys: ', tempKeys);
	tempKeys.sort(); // need the keys to be aligned alphabetically
	console.log('tempKeys: ', tempKeys);
	tempKeys.forEach(function (key) {
		if (tempParams[key] === "REFUND" || tempParams[key] === "|") {
			// tempParams[key] = "";
			return;
		}
		if (key !== 'CHECKSUMHASH') {
			if (tempParams[key] === 'null') tempParams[key] = '';
			if (!mandatoryflag || mandatoryparams.indexOf(key) !== -1) {
				data += (tempParams[key] + '|');
			}
		}
	});
	console.log('data: ', data);
	return data;
}

const genchecksum = (params, key, cb) => {
	let flag = 0;
	const data = paramsToString(params);
	// console.log('data: ', data);
	if (params.TXNTYPE !== undefined || params.TXNTYPE) {
		flag = (params.TXNTYPE === 'REFUND') ? 1 : 0;
	}
	crypt.gen_salt(4, function (err, salt) {
		const sha256 = crypto.createHash('sha256').update(data + salt).digest('hex');
		const check_sum = sha256 + salt;
		const encrypted = crypt.encrypt(check_sum, key);
		if (flag) {
			params.CHECKSUM = encrypted;
		} else {
			params.CHECKSUMHASH = encrypted;
		}
		console.log('params: ', params);
		cb(undefined, params);
	});
}

const paramsToStringrefund = (params, mandatoryflag) => {
	let data = '';
	const tempKeys = Object.keys(params);
	tempKeys.sort();
	tempKeys.forEach(function (key) {
		if (params[key].indexOf("|") > -1) {
			params[key] = "";
		}
		if (key !== 'CHECKSUMHASH') {
			if (params[key] === 'null') params[key] = '';
			if (!mandatoryflag || mandatoryParams.indexOf(key) !== -1) {
				data += (params[key] + '|');
			}
		}
	});
	return data;
};

const genchecksumforrefund = (params, key, cb) => {
	const data = paramsToStringrefund(params);
	crypt.gen_salt(4, function (err, salt) {
		const sha256 = crypto.createHash('sha256').update(data + salt).digest('hex');
		const check_sum = sha256 + salt;
		const encrypted = crypt.encrypt(check_sum, key);
		params.CHECKSUM = encodeURIComponent(encrypted);
		cb(undefined, params);
	});
};

const verifychecksum = (params, key) => {
	const data = paramsToString(params, false);
	//TODO: after PG fix on thier side remove below two lines
	if (params.CHECKSUMHASH) {
		params.CHECKSUMHASH = params.CHECKSUMHASH.replace('\n', '');
		params.CHECKSUMHASH = params.CHECKSUMHASH.replace('\r', '');

		const temp = decodeURIComponent(params.CHECKSUMHASH);
		console.log('temp: ', temp);
		const checksum = crypt.decrypt(temp, key);
		console.log('checksum: ', checksum);
		const salt = checksum.substr(checksum.length - 4);
		const sha256 = checksum.substr(0, checksum.length - 4);
		const hash = crypto.createHash('sha256').update(data + salt).digest('hex');
		if (hash === sha256) {
			return true;
		} else {
			util.log("checksum is wrong");
			return false;
		}
	} else {
		util.log("checksum not found");
		return false;
	}
};

module.exports = {
	genchecksum,
	verifychecksum,
	genchecksumforrefund
}