"use strict";

var crypt = require('./crypt');
var util = require('util');
var crypto = require('crypto');

//mandatory flag: when it set, only mandatory parameters are added to checksum

const paramsToString = (params, mandatoryflag)=> {
	let data = '';
	const flag = params.refund ? true : false;
	let value = "";
	delete params.refund;
	const tempKeys = Object.keys(params);
	// if (!flag) 
	tempKeys.sort();
	tempKeys.forEach((key)=> {
		value = params[key];
		if (value.indexOf("REFUND") > -1 || value.indexOf("|") > -1) {
			return ;
		}
		if (key !== 'CHECKSUMHASH' ) {
			if (params[key] === 'null') params[key] = '';
			if (!mandatoryflag || mandatoryParams.indexOf(key) !== -1) {
				data += (params[key] + '|');
			}
		}
	});
	console.log('data: ', data);
	return data;
}


const genchecksum = (params, key, cb)=> {
	const flag = params.refund ? true : false;
	const data = paramsToString(params);
	crypt.gen_salt(4, (err, salt)=> {
		const sha256 = crypto.createHash('sha256').update(data + salt).digest('hex');
		const check_sum = sha256 + salt;
		const encrypted = crypt.encrypt(check_sum, key);
		if (flag) {
			params.CHECKSUM = encodeURIComponent(encrypted);
		} else {
			params.CHECKSUMHASH = encodeURIComponent(encrypted);
		}
		const keys = Object.keys(params);
		let tempObj = {};
    for(key of keys) {
        tempObj[key] = params[key];
    };
		console.log('\ntempObj: ', tempObj);
		cb(null, tempObj);
	});
}


const verifychecksum = (params, key)=> {

	if (!params) {
		console.log("params are null");
	}
	const data = paramsToString(params, false);
	//TODO: after PG fix on thier side remove below two lines
	if (params.CHECKSUMHASH) {
		params.CHECKSUMHASH = params.CHECKSUMHASH.replace('\n', '');
		params.CHECKSUMHASH = params.CHECKSUMHASH.replace('\r', '');

		let temp = decodeURIComponent(params.CHECKSUMHASH);
		var checksum = crypt.decrypt(temp, key);
		var salt = checksum.substr(checksum.length - 4);
		var sha256 = checksum.substr(0, checksum.length - 4);
		var hash = crypto.createHash('sha256').update(data + salt).digest('hex');
		if (hash === sha256) {
			return true;
		} else {
			util.log("checksum is wrong");
			return false;
		}
	} else {
		util.log("checksum not found");
		return false;
	}
}

module.exports = {
	genchecksum,
	verifychecksum,
}